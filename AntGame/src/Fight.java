
public class Fight{
	
	/**
	 * How many ants surrounding a different colour ant.
	 * @param position Position of ant.
	 * @param colour Colour of enemy ant.
	 * @return
	 */
	public static int adjacentAnts(int[] position, String colour){
		int count = 0;
		//for all directions
		for(int d = 0; d <= 5; d++){
			int[] aCell = World.adjacentCell(position, d);
			if(World.someAntIsAt(aCell) && World.antAt(aCell).getColour().equals(colour)){
				count++;}}
		return count;
	}
	
	/**
	 * Check if ant at position is surrounded.
	 * @param position Position of ant being checked.
	 */
	public static void checkForSurroundedAntAt(int[] position){
		int food = 0;
		if(World.someAntIsAt(position)){
			Ant a = World.antAt(position);
			if(adjacentAnts(position, World.otherColour(a.getColour())) >= 5){
				//Killed ant becomes 3 particles of food
				food = 3;
				//if the killed ant was holding food, add that particle
				if(a.hasFood()){
					food++;}
				//Update world
				World.killAntAt(position);
				World.setFoodAt(position, World.foodAt(position) + food);
				a.killAnt();}}
	}
	
	/**
	 * Check for any surrounded ants adjacent to newly moved ant.
	 * Also checks if moved ant is surrounded.
	 * @param position Position of moved ant.
	 */
	public static void checkForSurroundedAnts(int[] position){
		//check if moved ant surrounded
		checkForSurroundedAntAt(position);
		//check if any adjacent ants surrounded
		for(int d = 0; d <= 5; d++){
			checkForSurroundedAntAt(World.adjacentCell(position, d));
		}
	}
	
}