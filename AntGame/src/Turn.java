
public class Turn {
	
	private int direction;
	
	/**
	 * Turns ant. 
	 * @param direction Current direction of ant
	 */
	public Turn(int direction){
		this.direction = direction;}
	
	/**
	 * Calculate new direction
	 * @param lr Left or Right
	 * @return The new direction
	 */
	public int turnLR(String lr){
		if(lr.equalsIgnoreCase("Left")){
			direction = (direction + 5) % 6;}
		else if(lr.equalsIgnoreCase("Right")){
			direction = (direction + 1) % 6;}
		else{
			try {
				throw new Exception("Input not left or right");
			} catch (Exception e) {}
		}
		return direction;}
}
