import java.util.ArrayList;
import java.util.Random;

public class AntControl{

	private ArrayList<String> blackBrain;
	private ArrayList<String> redBrain;
	private Random random = new Random();
	
	public AntControl(ArrayList<String> blackBrain, ArrayList<String> redBrain){
		this.blackBrain = blackBrain;
		this.redBrain = redBrain;}
	
	//Get the instruction for an ant, depends on its colour and state
	public String getInstruction(String colour, int state){
		String instruction = null;
		if(colour.equals("Red")){
			instruction = redBrain.get(state);}
		else if(colour.equals("Black")){
			instruction = blackBrain.get(state);}
		return instruction;}
	
	//Step function, controls the ant according to instructions
	public void step(Ant ant, String instructions){
		String instr[] = instructions.split(" ");
		if(ant.isAlive()){
			String token = instr[0];
			int[] pos = ant.getPosition();
			//Check if resting
			if(ant.getResting() > 0){
				ant.updateResting();}
			else{
				//senses a specific cell for a specific thing
				if(token.equalsIgnoreCase("Sense")){
					Sense s = new Sense(instr);
					//Gets the coordinates of the cell being sensed
					int[] senseCell = World.sensedCell(pos, ant.getDirection(), instr[1]);
					if(s.cellMatches(senseCell, instr[4], ant.getColour())){
						ant.setState(Integer.parseInt(instr[2]));}
					else{
						ant.setState(Integer.parseInt(instr[3]));}}
					
				//marks cell with own marker
				else if(token.equalsIgnoreCase("Mark")){
					//check if marker available from ant's colour, marker number
					if(!Marker.markerUsed(ant.getColour(), Integer.parseInt(instr[1]))){
						//check there isn't already a marker there
						if(!(World.getCell(pos)).marker()){
							Marker.setMarkerAt(pos, ant.getColour(), Integer.parseInt(instr[1]));}}
					ant.setState(Integer.parseInt(instr[2]));}
				
				//clears own marker
				else if(token.equalsIgnoreCase("Unmark")){
					//Check the correct marker is in the cell
					if(Marker.checkMarkerAt(pos, ant.getColour(), Integer.parseInt(instr[1]))){
						Marker.clearMarkerAt(pos, ant.getColour(), Integer.parseInt(instr[1]));}
					ant.setState(Integer.parseInt(instr[2]));}
				
				//picks up food if it can
				else if(token.equalsIgnoreCase("PickUp")){
					//if the ant already has food, or there is no food where it is
					if(ant.hasFood() | World.foodAt(pos) == 0){
						ant.setState(Integer.parseInt(instr[2]));}
					//else pick up one piece of food, update food in cell
					else{
						ant.setFood(true);
						ant.setState(Integer.parseInt(instr[1]));
						World.setFoodAt(pos, (World.foodAt(pos) - 1));}}
				
				//if ant is holding food, drop it, update cell it is dropped in
				else if(token.equalsIgnoreCase("Drop")){
					if(ant.hasFood()){
						World.setFoodAt(pos, (World.foodAt(pos) + 1));
						ant.setFood(false);}
					ant.setState(Integer.parseInt(instr[1]));}
				
				//Turn the ant left or right
				else if(token.equalsIgnoreCase("Turn")){
					Turn t = new Turn(ant.getDirection());
					//calculate and set the new direction of the ant
					ant.setDirection(t.turnLR(instr[1]));
					ant.setState(Integer.parseInt(instr[2]));}
			
				//Moves an ant then checks if an ant is surrounded
				else if(token.equalsIgnoreCase("Move")){
					int[] newPos = World.adjacentCell(pos, ant.getDirection());
					
					//if cell it wants to move to is rocky or occupied by another ant
					if((World.rocky(newPos)) | (World.someAntIsAt(newPos))){
						ant.setState(Integer.parseInt(instr[2]));}
					else{
						//move ant to new cell
						World.clearAntAt(newPos);
						World.setAntAt(pos, ant.getId());
						ant.setState(Integer.parseInt(instr[1]));
						ant.updatePosition(newPos);
						//Sets up counter for resting to 14
						ant.setResting();
						//Check if this movement has an ant surrounded
						Fight.checkForSurroundedAnts(newPos);}}
				
				//Generates a random number to set the state of the ant
				else if(token.equalsIgnoreCase("Flip")){
					//Generates the random number
					int i = random.nextInt();
					//Decides state depending on if it is > 0 or < 0
					if(i <= 0){
						ant.setState(Integer.parseInt(instr[2]));}
					else{
						ant.setState(Integer.parseInt(instr[3]));}}
				}
			}
	}
}
