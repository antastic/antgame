
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class FinishedGui extends JFrame {

	private JPanel contentPane = new JPanel();
	private JTextField txtPleaseImportAn;
	private JTextField txtPlayer1;
	private JTextField txtPlayer2;
	JButton ContinueToGame = new JButton("Play Again");
	JMenuBar menuBarP1 = new JMenuBar();
	JPanel topPanel = new JPanel();
	JPanel panel = new JPanel();
	
	public GUI gui;
	
	
	String R;
	String B;
	
	
	
	JLabel P2winner = new JLabel("");
	JLabel P1winner = new JLabel("");
	JLabel messageP1 = new JLabel("Food Collected:");
	JLabel messageP2 = new JLabel("Food Collected:");
	JLabel P1score = new JLabel("");
	JLabel P2score = new JLabel("");
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					String R = ("");
					String B = ("");
					FinishedGui frame = new FinishedGui(R, B);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Takes a parameter R for Red ant Score B for black Ant
	 */
	public FinishedGui(String R, String B) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		
		topPanel.setBounds(10, 11, 414, 54);
		contentPane.add(topPanel);
		topPanel.setLayout(null);
		
		txtPleaseImportAn = new JTextField();
		txtPleaseImportAn.setBackground(SystemColor.activeCaption);
		txtPleaseImportAn.setFont(new Font("Tahoma", Font.PLAIN, 16));
		txtPleaseImportAn.setHorizontalAlignment(SwingConstants.CENTER);
		txtPleaseImportAn.setBounds(0, 5, 414, 38);
		txtPleaseImportAn.setText("Game Statistics");
		topPanel.add(txtPleaseImportAn);
		txtPleaseImportAn.setColumns(10);
		
		panel.setForeground(new Color(0, 0, 0));
		panel.setBounds(10, 61, 403, 190);
		contentPane.add(panel);
		panel.setLayout(null);
		
		txtPlayer1 = new JTextField();
		txtPlayer1.setBackground(new Color(173, 255, 47));
		txtPlayer1.setHorizontalAlignment(SwingConstants.CENTER);
		txtPlayer1.setFont(new Font("Tekton Pro Cond", Font.PLAIN, 16));
		txtPlayer1.setText("Red Team");
		txtPlayer1.setBounds(25, 11, 126, 35);
		panel.add(txtPlayer1);
		txtPlayer1.setColumns(10);
		
		txtPlayer2 = new JTextField();
		txtPlayer2.setText("Black Team");
		txtPlayer2.setHorizontalAlignment(SwingConstants.CENTER);
		txtPlayer2.setFont(new Font("Tekton Pro Cond", Font.PLAIN, 16));
		txtPlayer2.setColumns(10);
		txtPlayer2.setBackground(new Color(173, 255, 47));
		txtPlayer2.setBounds(248, 11, 126, 35);
		panel.add(txtPlayer2);
		
		
		messageP1.setForeground(new Color(255, 0, 0));
		messageP1.setHorizontalAlignment(SwingConstants.CENTER);
		messageP1.setBounds(10, 72, 110, 14);
		panel.add(messageP1);
		
		
		ContinueToGame.setBounds(105, 138, 119, 41);
		ContinueToGame.addActionListener(new PlayAgain());
		panel.add(ContinueToGame);
		
		messageP2.setHorizontalAlignment(SwingConstants.CENTER);
		messageP2.setForeground(Color.RED);
		messageP2.setBounds(224, 72, 126, 14);
		panel.add(messageP2);
		
		
		P1score.setBounds(105, 72, 46, 14);
		P2score.setBounds(328, 72, 46, 14);
		P1winner.setFont(new Font("Tahoma", Font.PLAIN, 20));
		P1winner.setHorizontalAlignment(SwingConstants.CENTER);
		P1winner.setBounds(25, 46, 126, 26);
		panel.add(P1winner);
		
		
		P2winner.setHorizontalAlignment(SwingConstants.CENTER);
		P2winner.setFont(new Font("Tahoma", Font.PLAIN, 20));
		P2winner.setBounds(248, 46, 126, 26);
		panel.add(P2winner);
		
		
		
		this.R = R;
		this.B = B;
		P1score.setText(R);
		P2score.setText(B);
		panel.add(P1score);
		panel.add(P2score);
		
		declareWinner();
		
		
	
		
	}
	public void declareWinner()
	{	
		//check if which colour is winner if Red
		//if(){
		P1winner.setText("WINNER");
		P2winner.setText("LOSER");
		//}
		//else { 
		P2winner.setText("WINNER");
		P1winner.setText("LOSER");
		//}
	}
	

		
		class PlayAgain implements ActionListener {
	        public void actionPerformed(ActionEvent ae) {
	        	setVisible(false);
	        	gui = new GUI();
	        	gui.setVisible(true);
	        }
		}
}
	          
	    