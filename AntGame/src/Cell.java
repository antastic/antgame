public class Cell{
	
	private boolean aRock;
	private boolean anAnt;
	private boolean antHill;
	private boolean marker;
	
	private Ant ant;
	private int numFood;
	private String hillColour;
	private String markColour;
	private int markNum;
	
	public Cell(){
		//Empty, only there so Cells can be set up
	}
	
	//------------------------------\\
	// Storage and return of a rock \\
	//------------------------------\\
	public boolean rock(){
		return aRock;}
	
	public void setRock(){
		aRock = true;}
	
	//-----------------------------------\\
	// Stores and anthill and its colour \\
	//-----------------------------------\\
	public boolean antHill(){
		return antHill;}
	
	public void setAntHill(boolean antHill, String colour){
		this.antHill = antHill;
		hillColour = colour;}
	
	public String getHillColour(){
		return hillColour;}	
	
	//---------------------------------\\
	// Storing, returning and checking \\
	// for an Ant in the cell          \\
	//---------------------------------\\
	public boolean anAnt(){
		return anAnt;}
	
	public void setAnt(Ant ant){
		anAnt = true;
		this.ant = ant;}
	
	public Ant getAnt(){
		return ant;}
	
	public void removeAnt(){
		ant = null;
		anAnt = false;}
	
	//---------------------------------\\
	// Stores marker colour and number \\
	// returns when requested.         \\
	//---------------------------------\\
	public boolean marker(){
		return marker;}
	
	public void setMarker(String colour, int num){
		markColour = colour;
		markNum = num;
		marker = true;}
	
	public void removeMarker(){
		markColour = null;
		//-1 to signify a marker number does not exist
		markNum = -1;
		marker = false;}
	
	public String getMarkColour(){
		return markColour;}
	
	public int getMarkNum(){
		return markNum;}
	
	//-------------------------\\
	// Deals with storing food \\
	// and amount of food.     \\
	//-------------------------\\
	public void setFood(int num){
		numFood = num;}
	
	public int foodAt(){
		return numFood;}
	
}