
public class Marker {
	//Sets up both red and black markers as unused
	private static boolean[] redMarkers = {false, false, false, false, false, false};
	private static boolean[] blackMarkers = {false, false, false, false, false, false};
	private static boolean used;
	
	//To check if the marker number is available
	public static boolean markerUsed(String colour, int num){
		used = false;
		if(colour.equals("Red")){
			//if redMarkers[num] is true, ie. in use
			if(redMarkers[num]){
				used = true;}}
		else if(colour.equals("Black")){
			//if blackMarkers[num] is false, ie. in use
			if(blackMarkers[num]){
				used = true;}}
		return used;}
	
	//updates a marker to used or unused
	public static void updateMarker(String colour, int num, boolean mark){
		if(colour.equals("Red")){
			redMarkers[num] = mark;}
		else if(colour.equals("Black")){
			blackMarkers[num] = mark;}}
	
	//sets a marker at a specific position and colour
	public static void setMarkerAt(int[] position, String colour, int markerNum){
		Cell cell = World.getCell(position);
		cell.setMarker(colour, markerNum);
		updateMarker(colour, markerNum, true);}
	
	//removes a specific marker of a colour and number 
	public static void clearMarkerAt(int[] position, String colour, int markerNum){
		Cell cell = World.getCell(position);
		cell.removeMarker();
		updateMarker(colour, markerNum, false);}
	
	//return true for a marker of a specific colour and number
	public static boolean checkMarkerAt(int[] position, String colour, int markerNum){	
		boolean check = false;
		Cell cell = World.getCell(position);
		//check the marker colour and number both match, checks there is a marker
		String col = cell.getMarkColour();
		if((col != null) && (col.equals(colour)) && (cell.getMarkNum() == (markerNum))){
			check = true;}
		return check;}
	
	//returns true if there is a marker of specific colour, regardless of number
	public static boolean checkAnyMarkerAt(int[] position, String colour){
		boolean check = false;
		Cell cell = World.getCell(position);
		String col = cell.getMarkColour();
		if((col.equals(colour)) && (col != null)){
			check = true;}
		return check;}
}
