//THE WORLD PARSER SYNTAX FOLLOWS THAT
//OF THE 4 EXAMPLE WORDS PROVIDED ON
//STUDY DIRECT

import java.io.*;
import java.util.*;

public class WorldParser {
	private File worldFile;
	private Scanner readLine;
	private String[][] world;
	private static final String rock = "#";
	private static final String food = "5";
	private static final String bAntHill = "-";
	private static final String rAntHill = "+";
	private static final String emptyCell = ".";
	
	private static final int LINE_LENGTH = 150;
	private static final int NUM_LINES = 150;
	//Keep count of the number of lines of the world
	private int counter = 0;
	private boolean valid = true;
	
	private static final int FOOD_BLOBS = 11;
	private static final int NUM_ROCKS = 14;
	
	public WorldParser(String fileName){
		//set up the 2D array to be used in-game
		world = new String[150][150];
		worldFile = new File(fileName);
		try {
			readLine = new Scanner(worldFile);
		} catch (FileNotFoundException e) {
			System.out.println("The world file could not be found");}}

	//Read in a line and return as characters
	public char[] readLine(){
		String line = readLine.nextLine();
		char[] c = line.toCharArray();
		return c;}
	
	//returns a 2D array of the world for in-game
	public String[][] returnWorld() throws InvalidObjectException{
		if(!validWorld() | !checkFormat()){
			throw new InvalidObjectException("Invalid world file");}
		return world;}
	
	//-------------------------------\\
	// methods to validate the world \\
	//-------------------------------\\
	public boolean validWorld(){
		//check the first two lines of the file X, Y coordinates
		if((validateX(readLine.nextLine())) && (validateY(readLine.nextLine()))){
			valid = true;}
		else{
			valid = false;
			return valid;}
		
		//Check the actual world
		while(readLine.hasNext() && valid){
			char[] tokens = readLine();
			//keep count of the number of lines of the world
			if(!(counter <= 150)){
				valid = false;
				break;}
				
			//Check the first and last lines of the world
			//are rocky
			else if(counter == 0){
				for(int i = 0; i < tokens.length; i++){
					int j = 1;
					if(!parameter(tokens[i]) && !Character.isWhitespace(tokens[j])){
						valid = false;
						break;}
					j++;}}
			else if(counter == 150){
				for(int i = 0; i < tokens.length; i++){
					int j = 1;
					if(!Character.isWhitespace(tokens[i]) && !parameter(tokens[j]) ){
						valid = false;
						break;}
					j++;}}
			
			//for validating each line
			else if(tokens.length == (LINE_LENGTH*2)){
				//Check the rocky parameters and the first rock is followed by a whitespace
				if(parameter(tokens[0]) && Character.isWhitespace(tokens[1]) && parameter(tokens[(tokens.length)-2])){
					valid = validLine(tokens);}
				else{
					valid = false;
					return valid;}}
			updateWorld(counter, tokens);
			counter++;}
		return valid;}
	
	//Check the written x size is valid
	public boolean validateX(String line){
		boolean matches = false;
		if(line.equals(NUM_LINES + "")){
			matches = true;}
		return matches;}
	
	//Check the written y size is valid
	public boolean validateY(String line){
		boolean matches = false;
		if(line.equals(LINE_LENGTH + "")){
			matches = true;}
		return matches;}
	
	//Checks it is a rock, called for parameter checking
	public boolean parameter(char c){
		boolean matches = false;
		if(Character.toString(c).equals(rock)){
			matches = true;}
		return matches;}
	
	//Checking the validity of line
	public boolean validLine(char[] line){
		boolean matches = false;
		for(int i = 2; i < line.length; i++){
			char c = line[i];
			char white = line[++i];
			if(validChar(c) && Character.isWhitespace(white)){
				matches = true;}
			else{
				matches = false;
				break;}}
		return matches;}
	
	//Checks if the values inside the rocky parameter are valid
	public boolean validChar(char c){
		boolean matches = false; 
		String token = Character.toString(c);
		if(token.equals(rock)){
			matches = true;}		
		else if(token.equals(food)){
			matches = true;}
		else if(token.equals(bAntHill)){
			matches = true;}
		else if(token.equals(rAntHill)){
			matches = true;}
		else if(token.equals(emptyCell)){
			matches = true;}
		return matches;}
	
	//Gives tokens coordinates
	public void updateWorld(int x, char[] line){
		int y = 0;
		for(int i = 0; i < line.length; i++){
			if(!Character.isWhitespace(line[i])){
				String s = Character.toString(line[i]);
				world[x][y] = s;
				y++;}}}
	
	
	//-----------------------------------\\
	// Methods for checking the 2D array \\
	// contents are correct       	     \\
	//-----------------------------------\\	
	//For checking the format of the food, anthills and num rocks
	public boolean checkFormat(){
		boolean rHill = false;
		boolean bHill = false;
		boolean correct = false;
		int numBlobs = 0;
		int rockCount = 0;
		
		//Only check inside rocky parameter
		for(int i = 1; i < LINE_LENGTH-1; i++){
			for(int j = 1; j < NUM_LINES-1; j++){
				String s = world[i][j];
				if(s.equals(rock) && rockCount <= 14){
					rockCount++;}
				else if(s.equals(rock) && rockCount == 14){
					correct = false;
					return correct;}
				//if a row of cells directly above contains row, it has been counted
				else if(s.equals(food) && numBlobs <= FOOD_BLOBS){
					if(!checkAbove(i, j, 5, food)){
						numBlobs++;
						correct = checkFoodFormat(i,j);
						//moves j to after that row of food
						j = j + 4;}}
				//Check there is only one ant hill of each colour
				//check they are acceptable size and shape
				else if(s.equals(bAntHill)){
					//Check black anthill hasn't already been parsed
					if(!checkAbove(i,j,7, bAntHill) && !bHill){
						bHill = true;
						correct = topHill(i,j,bAntHill);
						//moves j to after the antHill
						j = j + 6;}}
				else if(s.equals(rAntHill)){
					//Check red anthill hasn't already been parsed
					if(!checkAbove(i,j,7, rAntHill) && !rHill){
						rHill = true;
						correct = topHill(i,j,rAntHill);
						//moves j to after the anthill
						j = j + 76;}}}}
		//if not enough food or no anthills or enough rocks
		if(numBlobs < FOOD_BLOBS | !rHill | !bHill | rockCount < NUM_ROCKS){
			correct = false;}
		return correct;}
	
	//Checks the cells directly above the row in question
	public boolean checkAbove(int x, int y, int length, String obj){
		boolean exists = false;
		//x coord of row above
		x--;
		for(int i = 0; i < length; i++){
			if((world[x][y]).equals(obj)){
				y++;
				exists = true;
				break;}}
		return exists;}
	
	//Will check top half of hill is roughly correct shape 
	public boolean topHill(int x, int y, String colour){
		boolean correct = false;
		boolean correctLine;
		int startLength = 7;
		int endX = x + 7;
		//3 cell leeway on one side
		int newY = y-3;
		int count;
		
		//For all lines to the middle, where 7 is the middle line length 13
		while(x < endX){
			correctLine = false;
			//get the first coordinate that contains anthill on that line
			y = newY;
			count = 0;
			for(int i = 0; i < (startLength + 1); i++){
				//check for first coord and that a correct line not already checked
				if(!(world[x][y]).equals(colour) && !correctLine && count < 3){
					y++;
					count++;}
				//Will not allow leeway > 3 spaces
				else if(!(world[x][y]).equals(colour) && !(count < 3)){
					correct = false;
					return correct;}
				//check following cell after correctline not anthill
				else if((world[x][y]).equals(colour) && correctLine){
					correct = false;
					return correct;}
				else if((world[x][y]).equals(colour)){
					int k = 0;
					//This will make sure it is the correct length
					for(int j = y; j < y + startLength; j++){
						//if it is not, break out of all the loops 
						if(!(world[x][j]).equals(colour)){
							correctLine = false;
							correct = false;
							return correct;}
						else{
							correctLine = true;
							correct = true;
							k = j;}}
					//to make sure no anthill comes after
					if((world[x][k+1]).equals(colour)){
						correct = false;
						return correct;}
					else{
						break;}}}
			x++;
			startLength++;}
		
		//Check the bottom half of anthill correct
		if(bottomHill(endX, newY, colour)){
			correct = true;}
		else{
			correct = false;}
		return correct;}
	
	//Check bottom of hill roughly correct shape
	public boolean bottomHill(int x, int y, String colour){
		boolean correct = false;
		boolean correctLine;
		int startLength = 12; 
		int endX = x + 6;
		int newY = y;
		int count;
		
		//For all lines to the middle, where 7 is the middle line length 13
		while(x < endX){
			correctLine = false;
			//get the first coordinate that contains anthill on that line
			y = newY;
			count = 0;
			for(int i = 0; i < (startLength + 1); i++){
				//check for first coord and that a correct line not already checked
				if(!(world[x][y]).equals(colour) && !correctLine && count < 3){
					y++;
					count++;}
				//Will not allow leeway > 3 spaces
				else if(!(world[x][y]).equals(colour) && !(count < 3)){
					correct = false;
					return correct;}
				//check cell following correct line not anthill
				else if((world[x][y]).equals(colour) && correctLine){
					correct = false;
					return correct;}
				else if((world[x][y]).equals(colour)){
					int k = 0;
					//This will make sure it is the correct length
					for(int j = y; j < y + startLength; j++){
						//if it is not, break out of all the loops 
						if(!(world[x][j]).equals(colour)){
							correctLine = false;
							correct = false;
							return correct;}
						else{
							correctLine = true;
							correct = true;
							k = j;}}
					//to make sure no anthill comes after
					if((world[x][k+1]).equals(colour)){
						correct = false;
						return correct;}
					else{
						break;}}}
			x++;
			//shorten what the length of the line should be
			startLength--;}	
			return correct;}
	
	//Check food rectangle
	public boolean checkFoodFormat(int x, int y){
		boolean correct = true;
		int currentLength = 0;
		int length = 5;
		//2 cell leeway
		int newY = y - 2;
		int count = 0;
		y = newY;
		while(currentLength < length){
			//Check for first cell of food, leeway 2
			if(!(world[x][y]).equals(food) && count < 2){
				count++;
				y++;
				newY = y - 1;}
			else if(!(count <= 2)){
				correct = false;
				return correct;}
			//Check the size of each line
			else if(world[x][y].equals(food)){
				count = 0;
				for(int j = 0; j < length; j++){
					if(!(world[x][y]).equals(food)){
						correct = false;
						return correct;}}
				y = newY;
				x++;}
			currentLength++;}
		return correct;}
}
