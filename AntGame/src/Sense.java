
public class Sense {
	private String[] instr;
	public Sense(String[] instr){
		this.instr = instr;}
	
	public boolean cellMatches(int pos[], String condition, String colour){
		boolean matches = false;
		
		//Checks if position pos is a rocky cell
		if(condition.equalsIgnoreCase("Rock")){
			if(World.rocky(pos)){
				matches = true;}}
				
		//Check if ant at position pos and if that ant is the same colour
		else if(condition.equalsIgnoreCase("Friend")){
			if((World.someAntIsAt(pos)) && 
					((World.antAt(pos).getColour()).equalsIgnoreCase(colour))){
					matches = true;}}
		
		//Check if ant at a position pos and if that ant is not the same colour
		else if(condition.equalsIgnoreCase("Foe")){
			if((World.someAntIsAt(pos)) && 
						(!(World.antAt(pos).getColour()).equalsIgnoreCase(colour))){
					matches = true;}}
			
			//Check if ant is at position pos, if that ant is same colour and it has food
		else if(condition.equalsIgnoreCase("FriendWithFood")){
			if((World.someAntIsAt(pos)) && ((World.antAt(pos).getColour()).equalsIgnoreCase(colour))
					&& World.antAt(pos).hasFood()){
				matches = true;}}
			
		//check if ant at position p, if that ant is not same colour and it has food
		else if(condition.equalsIgnoreCase("FoeWithFood")){
			if((World.someAntIsAt(pos)) && (!(World.antAt(pos).getColour()).equalsIgnoreCase(colour))
					&& World.antAt(pos).hasFood()){
				matches = true;}}
		
		//check if there is food
		else if(condition.equalsIgnoreCase("Food")){
			if(World.foodAt(pos) > 0){
				matches = true;}}
		
		//Check for own marker
		else if(condition.equalsIgnoreCase("Marker")){
			if(Marker.checkMarkerAt(pos, colour, (instr.length)-1)){
				matches = true;}}
		
		//Check for foe marker
		else if(condition.equalsIgnoreCase("FoeMarker")){
		   if(Marker.checkAnyMarkerAt(pos, World.otherColour(colour))){
				matches = true;}}
		
		//Check if cell is home anthill
		else if(condition.equalsIgnoreCase("Home")){
			if(World.anthillAt(pos, colour)){
				matches = true;}
		
		//check if cell is enemy anthill
		else if(condition.equalsIgnoreCase("FoeHome")){
			if(World.anthillAt(pos, World.otherColour(colour))){
				matches = true;}}
		}
		return matches;
	}

}
