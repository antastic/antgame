import java.io.File;
import java.io.InvalidObjectException;
import java.util.ArrayList;

public class Game {
	//---------------------------------------------------\\
	//USERS PLEASE ADD THE NAME AND PATH OF THE FILE HERE \\
	private String worldFile = "3.world";
	//----------------------------------------------------\\
	//----------------------------------------------------\\
	
	private String[][] world;
	private ArrayList<String> brain1;
	private ArrayList<String> brain2;
	private final int NUMROUNDS = 300000;
	private World aWorld;
	//for the x and y coordinates of the world
	private final int SIZE = 150;
	
	public static void main(String[] args){
//		GUI gui = new GUI();
		File f1 = new File("cleverbrain3.brain");
		File f2 = new File("cleverbrain1.brain");
		Game game = new Game(f1, f2);
	}
	
	//can be edited depending on the GUI
	public Game(File firstBrain, File secondBrain){
		AntParser ap1 = new AntParser(firstBrain);
		AntParser ap2 = new AntParser(secondBrain);
		WorldParser wp = new WorldParser(worldFile);
	
		//for now, first brain is always black, the other always red
		try {
			brain1 = ap1.getValidBrain();
		} catch (InvalidObjectException e) {
			System.out.println(firstBrain + " brain is invalid");}
		
		try {
			brain2 = ap2.getValidBrain();
		} catch (InvalidObjectException e) {
			System.out.println(secondBrain + " brain is invalid");}
		
		//try to get a valid world here
		AntControl antControl = new AntControl(brain1, brain2);
		//give the world a map as an arraylist
		try {
			world = wp.returnWorld();
		} catch (InvalidObjectException e) {
			System.out.println("The world is an invalid file");
		}
		//set up and populate world
		aWorld = new World(world, antControl);
		run();
		getWinner();
		}
	
	//The actual game starts with round 1, after all ants set up
	public void run(){
		long start =System.currentTimeMillis();
		for(int i = 1; i < NUMROUNDS; i++){
			aWorld.aRound();
			updateGUI();}
		System.out.println(System.currentTimeMillis() - start);
	}
	
	public void updateGUI(){
		
	}
	
	public void getWinner(){
		//will produce Draw, Black or Red
		String winner = aWorld.getWinner();
		System.out.println(winner);
		//count()
		//should get the winner of the game
	}

}
