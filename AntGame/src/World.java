import java.util.*;

public class World {
	private static ArrayList<Ant> ants = new ArrayList<Ant>();
	private static Cell[][] cells;
	private AntControl ac;
	
	//Store where the anthills are for scoring purposes
	private ArrayList<int[]> blackAntHill = new ArrayList<int[]>();
	private ArrayList<int[]> redAntHill = new ArrayList<int[]>();
	
	//takes 2 ant brains and an ant controller
	public World(String[][] world, AntControl ac){
		cells = new Cell[150][150];
		this.ac = ac;
		//Set up the world ready for the game
		populate(world, 150, 150);}
	
	//Creates cells and populates them if needed
	//sets up ants with an: 
	//id, colour, state, resting counter, no food, position
	public void populate(String[][] parsedWorld, int x, int y){
		int antId = 0;
		for(int i = 0; i < x; i ++){
			for(int j = 0; j < y; j++){
				String contents = parsedWorld[i][j];
				int[] coords = {i,j};
				//Creates new cell object to be put into world
				Cell c = new Cell();
				
				//Sets cell as rock
				if(contents.equals("#")){
					cells[i][j] = c;
					c.setRock();}
				
				//Sets empty cell
				else if(contents.equals(".")){
					cells[i][j] = c;}
				
				//Sets cell to red anthill
				//Adds anthill coords
				else if(contents.equals("+")){
					Ant ant = new Ant(antId, "Red", 0, 0, false, coords);
					cells[i][j] = c;
					c.setAntHill(true, "Red");
					c.setAnt(ant);
					redAntHill.add(coords);
					ants.add(ant);
					antId++;}
				
				//Sets cell to black anthill
				//Adds anthill coords
				else if(contents.equals("-")){
					Ant ant = new Ant(antId, "Black", 0, 0, false, coords);
					cells[i][j] = c;
					c.setAntHill(true, "Black");
					c.setAnt(ant);
					blackAntHill.add(coords);
					ants.add(ant);
					antId++;}
				
				//Sets food in cell to starting amount 5
				else if(contents.matches("[0-9]")){
					cells[i][j] = c;
					c.setFood(5);}}}}
	
	//Does something with all ants for 1 round of the game
	public void aRound(){
		for(int i = 0; i < ants.size(); i++){
			Ant anAnt = ants.get(i);
			String instruction =
					ac.getInstruction(anAnt.getColour(), anAnt.getState());
			ac.step(anAnt, instruction);
		}
	}
	
	//To get and return the winner or draw
	public String getWinner(){
		String winner = null;
		int red = 0;
		int black = 0;
		//both anthills same size
		for(int i = 0; i < redAntHill.size(); i++){
			int[] r = redAntHill.get(i);
			int[] b = blackAntHill.get(i);
			red = red + cells[r[0]][r[1]].foodAt(); 
			black = black + cells[b[0]][b[1]].foodAt();}
		//picks which side wins or if it is a draw
		if(black > red){
			winner = "Black";}
		else if(red > black){
			winner = "Red";}
		else{
			winner = "Draw";}
		return winner;
	}
	
	//-----------------------------------\\
	// Following methods are for getting \\
	// the contents of specific cells    \\
	//-----------------------------------\\ 
	//Returns the other colour depending on the input colour
	public static String otherColour(String c){
		String newC = "";
		if(c.equals("Red")){
			newC = "Black";}
		else if(c.equals("Black")){
			newC = "Red";}
		return newC;}
	
	//Gets the cell object stored at position
	public static Cell getCell(int[] pos){
		Cell cell = cells[pos[0]][pos[1]];
		return cell;}
	
	//See if a cell has a rock
	public static boolean rocky(int[] pos){
		boolean rocky = false;
		if(cells[pos[0]][pos[1]].rock()){
			rocky = true;}
		return rocky;}
	
	//check if there is an ant
	public static boolean someAntIsAt(int[] pos){
		boolean containsAnt = false;
		if(cells[pos[0]][pos[1]].anAnt()){
			containsAnt = true;}
		return containsAnt;}
	
	//Get the amount of food in a cell
	//if food < 1, there is no food
	public static int foodAt(int[] pos){
		int food = cells[pos[0]][pos[1]].foodAt();
		return food;}
	
	//Checks if the cell is part of an anthill
	public static boolean anthillAt(int[] pos, String colour){
		boolean matches = false;
		if(cells[pos[0]][pos[1]].antHill()){
			matches = true;}
		return matches;}
	
	//Check if there is an ant at a specific position
	public static Ant antAt(int[] pos){
		Ant ant = cells[pos[0]][pos[1]].getAnt();
		return ant;}
	
	//------------------------\\
	// For manipulating cells \\
	//------------------------\\
	//Sets how much food in particular cell
	public static void setFoodAt(int pos[], int numFood){
		cells[pos[0]][pos[1]].setFood(numFood);}
	
	//Sets and clears ants
	public static void clearAntAt(int pos[]){
		cells[pos[0]][pos[1]].removeAnt();}
	
	//check if this updates the position method in the ant as well!!!!!
	public static void setAntAt(int pos[], int id){
		Ant theAnt = ants.get(id);
		cells[pos[0]][pos[1]].setAnt(theAnt);}
	
	//checks if specific ant is alive
	public static boolean antIsAlive(int id){
		boolean alive = false;
		if(ants.get(id).isAlive()){
			alive = true;}
		return alive;}
	
	//find a particular ant
	public static int[] findAnt(int id){
		int[] pos = null;
		if(antIsAlive(id)){
			pos = ants.get(id).getPosition();}
		return pos;}
	
	//Removes ant
	public static void killAntAt(int pos[]){
		cells[pos[0]][pos[1]].removeAnt();}
		
	//------------------------------------------\\
	// For calculations involving cell position \\
	//------------------------------------------\\
	/**
	 * Calculate position of cell being sensed.
	 * @param pos Position of ant
	 * @param direction Direction ant currently facing
	 * @param senseDir Direction of cell to be sensed
	 * @return Coordinates of cell being sensed
	 */
	public static int[] sensedCell(int[] pos, int direction, String senseDir){
		int[] newPos = null;
		Turn t = new Turn(direction);
		if(senseDir.equalsIgnoreCase("Here")){
			newPos = pos;}
		else if(senseDir.equalsIgnoreCase("Ahead")){
			newPos = adjacentCell(pos, direction);}
		else if(senseDir.equalsIgnoreCase("LeftAhead") | senseDir.equalsIgnoreCase("RightAhead")){
			newPos = adjacentCell(pos, t.turnLR(senseDir));}
		return newPos;}
	
	/**
	 * Calculates adjacent cells coordinates.
	 * @param position The cell the adjacent cells are to be calculated
	 * @param direction Direction of cell
	 * @return Adjacent cell coordinates returned
	 */
	public static int[] adjacentCell(int[] position, int direction){
		int[] newPos = new int[2]; 
		int x = position[0];
		int y = position[1];
		
		switch(direction){
		case 0:
			newPos[0] = x + 1;
			newPos[1] = y;
			break;
			
		case 1:
			if(direction % 2 == 0){
				newPos[0] = x;
				newPos[1] = y + 1;}
			else{
				newPos[0] = x + 1;
				newPos[1] = y + 1;}
			break;
			
		case 2:
			if(direction % 2 == 0){
				newPos[0] = x - 1;
				newPos[1] = y + 1;}
			else{
				newPos[0] = x;
				newPos[1] = y + 1;}
			break;
			
		case 3:
			newPos[0] = x - 1;
			newPos[1] = y;
			break;
		
		case 4:
			if(direction % 2 == 0){
				newPos[0] = x - 1;
				newPos[1] = y - 1;}
			else{
				newPos[0] = x;
				newPos[1] = y - 1;}
			break;
		
		case 5:
			if(direction % 2 == 0){
				newPos[0] = x;
				newPos[1] = y - 1;}
			else{
				newPos[0] = x + 1;
				newPos[1] = y - 1;}
			break;
		}
	return newPos;
	}
}
