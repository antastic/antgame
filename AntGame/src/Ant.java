
public class Ant {

	private int id;
	private String colour;
	private int state;
	private int resting;
	private int direction;
	private int[] position = new int[2];
	
	private boolean food;
	private boolean alive;
	
	/**
	 * Updates and stores an ants information. Create an Ant
	 * @param id Ant's id number
	 * @param colour Colour of ant
	 * @param state Current state of ant
	 * @param resting Resting counter
	 * @param direction Direction ant is facing
	 * @param food If ant is holding food
	 * @param position Coordinates of cell ant is in
	 */
	public Ant(int id, String colour, int state, int direction, boolean food, int[] position){
		this.id = id;
		this.colour = colour;
		this.state = state;
		this.direction = direction;
		this.position = position;
		this.food = food;
		
		resting = 0;
		alive = true;}
	
	public void setState(int newState){
		state = newState;}
	
	public void setDirection(int newDirection){
		direction = newDirection;}
	
	public void setFood(boolean hasfood){
		food = hasfood;}
	
	//Set ant to resting for 14 turns
	public void setResting(){
		resting = 14;}
	
	//can only move if has rested for 14 turns
	public void updateResting(){
		resting--;}
	
	public void updatePosition(int[] newPos){
		position = newPos;}
	
	//Kill the ant
	public void killAnt(){
		alive = false;}
	
	//--------------\\
	// Get ant info \\
	//--------------\\
	public int getId(){
		return id;}
	
	public String getColour(){
		return colour;}

	public int getState(){
		return state;}
	
	public int getResting(){
		return resting;}
	
	public int getDirection(){
		return direction;}
	
	public int[] getPosition(){
		return position;}
	
	public boolean hasFood(){
		return food;}
	
	public boolean isAlive(){
		return alive;}
}
