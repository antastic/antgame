import java.io.*;
import java.util.*;

public class AntParser {
	private File antFile;
	private Scanner sc;
	private Scanner tokenScan;
	private ArrayList<String> states;
	private boolean ok = false;
	
	//CONDITIONS, SENSEDIR and TURN are all arrays of case-insensitive regex patterns
	private static final String[] CONDITIONS = 
		{"(?i)Friend", "(?i)Foe", "(?i)FriendWithFood", "(?i)FoeWithFood", "(?i)Food", "(?i)Rock", "(?i)Marker", "(?i)FoeMarker", "(?i)Home", "(?i)FoeHome"};
	private static final String[] SENSEDIR = {"(?i)Here", "(?i)Ahead", "(?i)LeftAhead", "(?i)RightAhead"};
	private static final String[] TURN = {"(?i)Left", "(?i)Right"};
	//Max number of states
	private static final int NUM_STATES = 10000;
	//Max number of markers an ant can have
	private static final int NUM_MARKERS = 6;
	
	/**
	 * Takes an input brain file and validates it.
	 * @param fileName The uploaded file
	 */
	public AntParser(File antFile){
		try {
			this.antFile = antFile;
			sc = new Scanner(new FileReader(antFile));
		} catch (FileNotFoundException e) {
			System.out.println("The ant brain file could not be found");}}
	
	//Returns a valid brain or throws an exception
	public ArrayList<String> getValidBrain() throws InvalidObjectException{
		if(!validity()){
			throw new InvalidObjectException("The input brain is invalid");}
		return states;}
	
	//provides GUI with true or false
	public boolean validity(){
		if(validBrain()){
			ok = true;}
		return ok;}
	
	//returns whether the brain is valid or not
	public boolean validBrain(){
		int stateCounter = 0;
		boolean valid = true;
		states = new ArrayList<String>();
		while(sc.hasNext()){
			//sets up a new scanner to validate a line of the file
			String nextLine = readLine();
			tokenScan = new Scanner(nextLine);
			//any invalid state will stop reading of the file
			if(!valid){
				break;}
			else if(stateCounter < NUM_STATES){
				//Will get the validity of each state
				valid = validState();
				
				//Checks after valid state found
				if(tokenScan.hasNext()){
					String checkNextToken = tokenScan.next();
					if((!checkNextToken.startsWith(";"))){
						valid = false;
						break;}}
				stateCounter++;
				addState(nextLine);}
			else{
				valid = false;
				break;}}
		return valid;}
	
	//Adds valid state to array of indexed states
	public void addState(String nextLine){
		String newState = "";
		String toSplit[];
		//Removes comments
		if(nextLine.contains(";")){
			toSplit = nextLine.split(";");
			newState = toSplit[0].trim();}
		else{
			newState = nextLine;}
		states.add(newState);}
	
	//-------------------------------------------------\\
	//The methods for validating a given ant brain are \\
	//given in the methods below.                      \\
	//-------------------------------------------------\\
	public String readLine(){
		String line = "";
		if(sc.hasNextLine()){
			line = sc.nextLine();}
		return line;}
	
	//For reading in a single token
	public String getToken(){
		String token = "";
		if(tokenScan.hasNext()){
			token = tokenScan.next();}
		return token;}
	
	//----------------------------------------\\
	// Checking if a state is valid.		  \\
	// If any part of the state being checked \\
	// is invalid, the state is invalid and	  \\
	// false validity will be returned. The   \\
	// state tokens are checked in order of   \\
	// their syntax.             		      \\
	//----------------------------------------\\
	public boolean validState(){
		boolean valid = true;
		String token = getToken();
		
		//Sense senseDirection stateNum1 stateNum2 condition
		if(token.matches("(?i)Sense")){
			if(validSenseDir(getToken()) && validStateNum(getToken()) && validStateNum(getToken()) && validCondition(getToken())){
				valid = true;}
			else{
				valid = false;}}
		
		//Mark markerNum stateNum1
		else if(token.matches("(?i)Mark") | token.matches("(?i)Unmark")){
			if(validMarker(getToken()) && validStateNum(getToken())){
				valid = true;}
			else{
				valid = false;}}
		
		//Validate 2 states in one 8D : 
		//(Pickup or Mark) stateNum1 stateNum2
		else if(token.matches("(?i)PickUp") | token.matches("(?i)Move")){
			if(validStateNum(getToken()) && validStateNum(getToken())){
				valid = true;}
			else{
				valid = false;}}
		
		//Drop stateNum1
		else if(token.matches("(?i)Drop")){
			if(validStateNum(getToken())){
				valid = true;}
			else{
				valid = false;}}
		
		//Turn turnLeftOrRight stateNum1
		else if(token.matches("(?i)Turn")){
			if(validTurn(getToken()) && validStateNum(getToken())){
				valid = true;}
			else{
				valid = false;}}
		
		//Flip num>0 stateNum1 stateNum2
		else if(token.matches("(?i)Flip")){
			String p = getToken();
			String s1 = getToken();
			String s2 = getToken();
			if(p.matches("[0-9]+") && validStateNum(s1) && validStateNum(s2)){
				valid = true;}
			else{
				valid = false;}}
		
		//If a state does not match the syntax of any of the above
		else{
			valid = false;}
		return valid;
	}
	
	//Checks the given token is a valid condition
	public boolean validCondition(String token){
		boolean valid = false;
		//if the token is Marker, check its number
		//Marker markerNumber
		if(token.matches("(?i)Marker")){
			valid = validMarker(getToken());}
		else{
			for(int i = 0; i < CONDITIONS.length; i++){
				if(token.matches(CONDITIONS[i])){
					valid = true;
					break;}
				else{
					valid = false;}}}
		return valid;}
	
	//Checks token is a direction it can sense
	public boolean validSenseDir(String token){
		boolean valid = false;
		for(int i = 0; i < SENSEDIR.length; i++){
			if(token.matches(SENSEDIR[i])){
				valid = true;
				break;}}
		return valid;}
	
	//Checks token is a valid turn direction
	public boolean validTurn(String token){
		boolean valid = false;
		for(int i = 0; i < TURN.length; i++){
			if(token.matches(TURN[i])){
				valid = true;
				break;}}
		return valid;}
	
	//Checks token is a valid marker number
	public boolean validMarker(String token){
		int i = Integer.parseInt(token);
		boolean valid = false;
		if(i < NUM_MARKERS){
			valid = true;}
		return valid;}
	
	//Checks token is a valid state number
	public boolean validStateNum(String token){
		int i = Integer.parseInt(token);
		boolean valid = false;
		if(i < NUM_STATES){
			valid = true;}
		return valid;}
}
