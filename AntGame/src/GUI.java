import java.awt.event.*;
import java.awt.*;
import java.io.*;
import javax.swing.*;

public class GUI extends JFrame {
	
	private static final int WIDTH = 400;
	private static final int HEIGHT = 400;
	
	
	public File P1file;
	public File P2file;

	public JFileChooser fileChooser = new JFileChooser();
	JButton button = new JButton("Continue");
	JTextField txtPleaseSelect = new JTextField();
	JTextField P1fileName = new JTextField();
	
	JButton P2Submit = new JButton("Check");
	JButton P1Submit = new JButton("Check");
	
	JTextField txtNotCorrect = new JTextField();
	JButton continueBtn = new JButton("Conitue to Game");
	JLabel lblPlayer2 = new JLabel("Player 2");
	JTextField P2fileNae = new JTextField();
	JLabel lblPlayer1 = new JLabel("Player 1");
	
	
	private final JButton P2chooseBtn = new JButton("Choose");
	private final JButton P1chooseBtn = new JButton("Choose");
	
	

	/**
	 * Launch the application.
	 */
	
		public static void main(String[] args) {
			GUI test = new GUI();
			
			
		}
		
	
	/**
	 * Gui Setup
	 */
	public GUI() {
		txtNotCorrect.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtNotCorrect.setHorizontalAlignment(SwingConstants.CENTER);
		txtNotCorrect.setText("Please try again AntBrain is incorrect!");
		txtNotCorrect.setBounds(0, 152, 434, 35);
		txtNotCorrect.setColumns(10);
		getContentPane().setBackground(new Color(143, 188, 143));
		txtNotCorrect.setEditable(false);
		
		getContentPane().setFont(new Font("Tahoma", Font.PLAIN, 14));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(null);
		txtPleaseSelect.setBounds(0, 0, 434, 35);
		txtPleaseSelect.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtPleaseSelect.setBackground(new Color(173, 216, 230));
		txtPleaseSelect.setForeground(new Color(0, 0, 0));
		txtPleaseSelect.setHorizontalAlignment(SwingConstants.CENTER);
		txtPleaseSelect.setText("Please Select an AntBrain to Import for each Player");
		txtPleaseSelect.setColumns(10);
		txtPleaseSelect.setEditable(false);
		getContentPane().add(txtPleaseSelect);
		P1fileName.setBounds(24, 79, 150, 20);
		P1fileName.setHorizontalAlignment(SwingConstants.CENTER);
		P1fileName.setText("FileName");
		P1fileName.setColumns(10);
		P1fileName.setEditable(false);
		getContentPane().add(P1fileName);
		
		continueBtn.setBounds(134, 190, 193, 38);
		continueBtn.addActionListener(new Continue());
		
		P2Submit.setBounds(274, 98, 150, 28);
		
		
		
        continueBtn.addActionListener(new Continue());
        
        
        
        lblPlayer1.setForeground(Color.RED);
        lblPlayer1.setBackground(Color.WHITE);
        lblPlayer1.setHorizontalAlignment(SwingConstants.CENTER);
        lblPlayer1.setFont(new Font("Tekton Pro Ext", Font.BOLD, 17));
        lblPlayer1.setBounds(42, 46, 102, 28);
        getContentPane().add(lblPlayer1);
        lblPlayer2.setHorizontalAlignment(SwingConstants.CENTER);
        lblPlayer2.setForeground(Color.RED);
        lblPlayer2.setFont(new Font("Tekton Pro Ext", Font.BOLD, 17));
        lblPlayer2.setBackground(Color.WHITE);
        lblPlayer2.setBounds(295, 46, 102, 28);
        
        getContentPane().add(lblPlayer2);
        P2fileNae.setText("FileName");
        P2fileNae.setHorizontalAlignment(SwingConstants.CENTER);
        P2fileNae.setEditable(false);
        P2fileNae.setColumns(10);
        P2fileNae.setBounds(274, 79, 150, 20);
        
        getContentPane().add(P2fileNae);
        P2chooseBtn.setBounds(295, 130, 89, 23);
        P2chooseBtn.addActionListener(new P2OpenAction());
        
        getContentPane().add(P2chooseBtn);
        P1chooseBtn.setBounds(54, 130, 89, 23);
        P1chooseBtn.addActionListener(new P1OpenAction());
        
        
        getContentPane().add(P1chooseBtn);
        P1Submit.setBounds(24, 98, 150, 28);
        
       
		
        
        
        
        
        
       
		setVisible(true);
		}
	
	class P1OpenAction implements ActionListener {
        public void actionPerformed(ActionEvent ae) {
            //... Open a file dialog.
            int retval = fileChooser.showOpenDialog(GUI.this);
            if (retval == JFileChooser.APPROVE_OPTION) {
            P1file = fileChooser.getSelectedFile();
            P1fileName.setText(P1file.getName());
            addP1Sumbit();
           
            
            }
        }
	}
	
	class P2OpenAction implements ActionListener {
        public void actionPerformed(ActionEvent p) {
            //... Open a file dialog.
            int retval = fileChooser.showOpenDialog(GUI.this);
            if (retval == JFileChooser.APPROVE_OPTION) {
            P2file = fileChooser.getSelectedFile();
            P2fileNae.setText(P2file.getName());
            addP2Sumbit();
            
            }
        }
	}
	
	class Action implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e) {
			//WindowBuilderTest test = new WindowBuilderTest();
			checkBrain();
			
			System.out.println("button was clicked.");
			
			
		}
	}
	
	
	class Continue implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent c) {
		
		setVisible(false);
			
			
			
			
		}
	}
	
	//... Get the file
	public File getP1File()
	{
		return P1file;
	}
	
	public File getP2File()
	{
		return P2file;
	}
	

	public void addP1Sumbit()
	{
		
		if(P1file != null){
			getContentPane().add(P1Submit);
			P1Submit.addActionListener(new Action());
		
			
		}
	}
	
	public void addP2Sumbit()
	{
		
		if(P1file != null && P2file != null){
			getContentPane().add(P2Submit);
			P2Submit.addActionListener(new Action());
		
			
		}
	}
	

		
	public void checkBrain()
	{
		//if Parser returns incorrect brain then:
		txtNotCorrect.setText("Please try again AntBrain is incorrect!");
		getContentPane().add(txtNotCorrect);
		
		//else:
		if(P2file == null) {
		txtNotCorrect.setText("The brain is correct, Upload player 2's");
		getContentPane().add(txtNotCorrect);
		}
		
		else if(P2file != null && P1file !=null)
		{
			txtNotCorrect.setText("both brains are correct, Please continue");
			getContentPane().add(txtNotCorrect);
			getContentPane().add(continueBtn);
			continueBtn.addActionListener(new Continue());
		}
	}
}
		
		